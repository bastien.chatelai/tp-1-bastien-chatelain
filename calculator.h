//
// Created by Bastien on 25.02.2022.
//

#ifndef TP_1_BASTIEN_CHATELAIN_CALCULATOR_H
#define TP_1_BASTIEN_CHATELAIN_CALCULATOR_H

// enum that describes the type of operator
enum class OperatorNname{addition, soustraction, multiplication, division, invalid};

/*
 * name : getOperatorNameFromChar
 * Description : return the OperatorName's enum that correspond to the parameter op
 * params : op (char, the operator)
 * return : enum OperatorName
 */
OperatorNname getOperatorNameFromChar(char op);

class Calculator{
    public :
        /*
        * name : constructor of class Calculator
        * params : a (float) = first operand, op (char) = operator, b = second operand (char)
        */
        Calculator(float a, char op, float b);

        /*
        * name : default constructor of class Calculator
        * params : -
        */
        Calculator();

        /*
        * name : destructor of class Calculator
        * params : -
        */
        ~Calculator();

        /*
        * name : calcul
        * Description : calcul with the 2 operands and the operator already set
        * params : -
        * return : result of the calcul (float)
        */
        float calcul();

        /*
        * name : setA
        * Description : set the value of the first operand (a)
        * params : second operand a (float)
        * return : -
        */
        void setA(float a);

        /*
        * name : getA
        * Description : get the value of the first operand
        * params : -
        * return : first operand (float)
        */
        float getA();

        /*
        * name : setB
        * Description : set the value of the second operand
        * params : second operand b (float)
        * return : -
        */
        void setB(float &b);

        /*
        * name : setOp
        * Description : set the value of the operator
        * params : operator (char)
        * return : -
        */
        void setOp(char op);

    private:
        // declaration of the operands
        float a_number;
        float b_number;
        // declaration of the operator
        OperatorNname op_operator;


};

#endif //TP_1_BASTIEN_CHATELAIN_CALCULATOR_H
