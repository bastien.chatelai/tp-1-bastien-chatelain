//
// Created by Bastien on 25.02.2022.
//

#include "calculator.h"

Calculator::Calculator(float a, char op, float b){
    this->a_number = a;
    this->b_number = b;
    // get the operator enum from the command getOperatorNameFromChar
    this->op_operator = getOperatorNameFromChar(op);
}

Calculator::Calculator(){
    // default value set
    this->a_number = 0.0;
    this->b_number = 0.0;
    this->op_operator = OperatorNname::invalid;
}

Calculator::~Calculator() {}


void Calculator::setA(float a){
    this->a_number = a;
}

float Calculator::getA() {
    return this->a_number;
}

void Calculator::setB(float &b){
    this->b_number = b;
}

void Calculator::setOp(char op){
    // get the operator enum from the command getOperatorNameFromChar
    this->op_operator = getOperatorNameFromChar(op);
}

float Calculator::calcul() {
    // declaration of the result of the calcul
    float result = 0.0;
    // switch statement to do the correct operation deponding on value op
    // result is set to -1 if there is an error
    switch(this->op_operator){
        case OperatorNname::addition:
            result = this->a_number + this->b_number;
            break;
        case OperatorNname::soustraction:
            result = this->a_number - this->b_number;
            break;
        case OperatorNname::multiplication:
            result = this->a_number * this->b_number;
            break;
        case OperatorNname::division: // TODO : exception handle better here
            // check if divison by zero
            if (this->b_number == 0){
                result = -1;
            } else {
                result = this->a_number / this->b_number;
            }

            break;
        default:
            result = -1;
    }
    return result;
}


OperatorNname getOperatorNameFromChar(char op){
    OperatorNname operatorNname;
    // switch statement to corresponds the parameter op with the enum OperatorName
    switch(op){
        case '+':
            operatorNname = OperatorNname::addition;
            break;
        case '-':
            operatorNname = OperatorNname::soustraction;
            break;
        case '*':
            operatorNname = OperatorNname::multiplication;
            break;
        case '/':
            operatorNname = OperatorNname::division;
            break;
        default:
            operatorNname = OperatorNname::invalid;
    }

    return operatorNname;
}