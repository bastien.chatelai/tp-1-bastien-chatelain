//
// Created by Bastien on 25.02.2022.
//

#include <iostream>
#include "calculator.h"

// TODO: better error handle
// TODO: try to create new CI/CD rules


// declaration of the function to check the operator's value
bool isOperatorValid(char op);

int main(int argc, const char ** argp){
    bool isRunning = true;

    // instanciate an calculator's object
    Calculator *calculator = new Calculator();

    // local variable for calcul
    // a and b : operand
    // op : operator
    float a = 0.0;
    char op = '0';
    float b = 0.0;

    // ask the user to enter a number
    std::cout << "Enter a decimal number (first operand of the calcul) : ";

    // user must enter a valid number (float)
    while (!(std::cin >> a)) {
        // clear the input stream
        std::cin.clear();
        std::cout << "Invalid number. Please enter a decimal number (first operand of the calcul) : ";
    }

    // set value a to the calculator
    calculator->setA(a);

    // main loop to make always a new calcul. It's end when user type # for the operator
    while(isRunning) {

        // print the first operand's value
        std::cout << std::endl << "First operand is : " << calculator->getA() << std::endl;

        // ask the user to enter the operator, then check if correct with function isOperatorValid()
        std::cout << std::endl << "Enter an operator (+ - * /) or # to exit : ";
        while(!(std::cin >> op) || !isOperatorValid(op)){
            // clear the input stream
            std::cin.clear();
            std::cout << std::endl << "Invalid operator. Enter an valid operator (+ - * /) or # to exit : ";
        }

        // stop programm if the user enter #
        if (op == '#'){
            isRunning = false;
            std::cout << std::endl << "Program exit";
            delete calculator;
            break;
        } else {
            // set the operator to the object calculator
            calculator->setOp(op);
        }

        // ask the user to enter the second operand, then check if the number is valid
        std::cout << std::endl << "Enter a decimal number (second operand of the calcul) : ";
        while (!(std::cin >> b)) {
            // clear the input stream
            std::cin.clear();
            std::cout << "Invalid number. Please enter a decimal number (second operand of the calcul) : ";
        }
        // set the second operand to the object calcultor
        calculator->setB(b);

        // print the result of the calcul with the function calcul()
        // then set the value result as value a to the calculator's object
        std::cout << std::endl << "Result of " << a << " " << op << " " << b << " = " << calculator->calcul()
                  << std::endl;
        calculator->setA(calculator->calcul());
    }
    return 0;
}

/*
 * name : isOperatorValid
 * Description : Check if the operator from the user's input is valid
 * params : op (char, the operator)
 * return : true (valid), false (invalid)
 */
bool isOperatorValid(char op){
    // check if the operator op is of type enum invalid
    // # is considered as valid because it's for exit programm
    if ((getOperatorNameFromChar(op) != OperatorNname::invalid) || (op == '#')){
        return true;
    }
    return false;
}